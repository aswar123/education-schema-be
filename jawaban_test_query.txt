- buatlah query untuk menampilkan peserta dengan gaji tertinggi
SELECT * FROM kawahedukasi ORDER BY gaji DESC LIMIT 1;
- buatlah query untuk menampilkan peserta BACKEND dengan nilai tertinggi dan gaji tertinggi
SELECT * FROM kawahedukasi WHERE kelas = "BACKEND" ORDER BY gaji DESC, nilai DESC LIMIT 1;
- buatlah query untuk melihat total peserta yang berada dalam kelas frontend
SELECT COUNT(*) as total_peserta FROM kawahedukasi WHERE kelas = 'FRONTEND';
- buatlah query untuk menampilkan jumlah kehadiran tertinggi dan nilai tertinggi dalam kelas frontend
SELECT * FROM kawahedukasi WHERE kelas = "FRONTEND" ORDER BY nilai DESC, jumlah_kehadiran DESC LIMIT 1;